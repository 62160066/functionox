/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mankhong.functionox;

import java.util.Scanner;

/**
 *
 * @author W.Home
 */
public class FunctionOX {
    static Scanner kb = new Scanner(System.in);
    static char winner = '-';  
    static int turn =0;
    static char[][] table = {
                      {'-','-','-'},
                      {'-','-','-'},
                      {'-','-','-'},
                      };
    static char player = 'X';
    static boolean isFinish = false;
    static void showWelcome(){
        System.out.println("Welcome to OX Game");
    }
    static void showTable(){
        System.out.println("  1 2 3");
        for(int row=0;row < table.length;row++){
                System.out.print(row+1+" ");
            for(int col=0;col < table[row].length;col++){
                System.out.print(table[row][col]+" ");
            }
            System.out.println(" ");
        }
    }
    static void showTurn(){
        System.out.println(player +" turn");             
    }
    static void checkWin(){
        checkRow();
        checkCol();
        checkDiagonal();
        if(turn == 9){
            isFinish = true;
        }
        
        
    }
    static void checkCol(){
        for(int row = 0;row < 3;row++){
            if(table[row][0]== table[row][1]&&table[row][1]==table[row][2]
            &&table[row][0]!= '-'){
                winner = table[row][0];
                isFinish = true;
                break;
            }
        }
    }
    static void checkRow(){
        for(int col = 0;col < 3;col++){
            if(table[0][col]== table[1][col]&&table[1][col]==table[2][col]
            &&table[0][col]!= '-'){
                winner = table[0][col];
                isFinish = true;
                break;
            }
        }
    }
    
    static void checkDiagonal(){
        if(table[0][0] == table[1][1] && table[1][1] == table[2][2]&&
        table[0][0]!= '-'){
            winner = table[0][0];
            isFinish = true;
        }else if(table[0][2] == table[1][1] && table[1][1] == table[2][0]&&
        table[0][2]!= '-'){
            winner = table[0][2];
            isFinish = true;
        }
    }
    static void switchPlayer(){
        if(player == 'X'){
            player = 'O';
            
        }else{
            player = 'X';
        }
    }
    static void inputPosition(){
        
        boolean repeat = false;
        do{ 
            System.out.println("Please input Row Col: ");
            int row = kb.nextInt()-1;
            int col = kb.nextInt()-1;
            if(table[row][col] != '-'){
                System.out.println("Error:This position is not Empty!!!");
                System.out.println("Please input again");
                repeat = true;
            }else{
                table[row][col] = player;
                repeat = false;
                turn++;
            }
        }while(repeat);
        
                
        
    }
    static void showWinner(){
              
        if(turn == 9){
            System.out.println("Draw!!");
            System.out.println("Bye Bye!!!");
        }else{
           System.out.println("Winner is "+winner+"!!!");
           System.out.println("Bye Bye!!!"); 
        }
        
    }    
    public static void main(String[] args) {
        showWelcome();
        showTable();
        do{
            showTurn();
            inputPosition();
            showTable();
            checkWin();
            switchPlayer();
        }while(!isFinish);
        showWinner();
        
        
        
    }
    
}
